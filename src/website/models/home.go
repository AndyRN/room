package models

// Home represents the home model for the view
type Home struct {
	Occupied bool
}
